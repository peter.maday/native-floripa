#include "hello/hello.hpp"

#include <iostream>

namespace hello {

std::string Message::get_message() const {
    return "Hello Native Floripa!";
}

void Message::show() const {
    std::cout << get_message() << '\n';
}

} // namespace hello
