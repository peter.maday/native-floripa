#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include "hello/hello.hpp"


TEST_CASE( "Validate Native Floripa", "[native]" ) {
    hello::Message hello;
    REQUIRE( hello.get_message().find("Floripa") != std::string::npos );
}
